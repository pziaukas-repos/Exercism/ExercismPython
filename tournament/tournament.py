from collections import Counter, defaultdict
from typing import List

WIN, DRAW, LOSS = 'win', 'draw', 'loss'
OPPONENT_OUTCOME = {WIN: LOSS, DRAW: DRAW, LOSS: WIN}

HEADER = ('Team', 'MP', 'W', 'D', 'L', 'P')
TEMPLATE = '{:<30} | {:>2} | {:>2} | {:>2} | {:>2} | {:>2}'


def tally(rows: List[str]) -> List[str]:
    teams = defaultdict(Counter)
    for row in rows:
        home, away, outcome = row.split(';')
        teams[home][outcome] += 1
        teams[away][OPPONENT_OUTCOME[outcome]] += 1

    summary = [(name, sum(card.values()), card[WIN], card[DRAW], card[LOSS], 3 * card[WIN] + card[DRAW])
               for name, card in teams.items()]
    summary.sort(key=lambda x: (-x[-1], x[0]))  # sort by higher point total and then alphabetically
    return [TEMPLATE.format(*row) for row in [HEADER] + summary]
