from typing import List


class Allergies(object):
    def __init__(self, score: int):
        self.item_map = {item: 2 ** k for k, item in
                         enumerate(['eggs', 'peanuts', 'shellfish', 'strawberries',
                                    'tomatoes', 'chocolate', 'pollen', 'cats'])}
        self.score = score

    def allergic_to(self, item: str) -> bool:
        return self.item_map[item] & self.score > 0

    @property
    def lst(self) -> List[str]:
        return [item for item, x in self.item_map.items() if x & self.score]
