def shift_symbol(symbol, key):
    if symbol.isalpha():
        offset = ord('A') if symbol.isupper() else ord('a')
        symbol = chr((ord(symbol) - offset + key) % 26 + offset)
    return symbol


def rotate(text, key):
    return ''.join(shift_symbol(symbol, key) for symbol in text)
