def leap_year(year: int) -> bool:
    if type(year) != int:
        raise Exception('Year must be an integer!')

    return ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0)
