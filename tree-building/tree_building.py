from typing import List, Union


class Record:
    def __init__(self, record_id: int, parent_id: int):
        self.record_id = record_id
        self.parent_id = parent_id


class Node:
    def __init__(self, node_id: int):
        self.node_id = node_id
        self.children = []


def BuildNode(id: int, family: List[List[bool]]) -> Node:
    node = Node(id)
    node.children = [BuildNode(child_id, family) for child_id in range(id + 1, len(family)) if family[child_id][id]]
    return node


def BuildTree(records: List[Record]) -> Union[Node, None]:
    if not records:
        return None

    family_size = max(x.record_id for x in records) + 1
    family = [[False]] + [[False] * k for k in range(1, family_size)]
    try:
        for record in records:
            family[record.record_id][record.parent_id] = True
        if not all(sum(parents) == 1 for parents in family):
            raise AttributeError
    except (IndexError, AttributeError):
        raise ValueError('Broken hierarchy!')

    return BuildNode(0, family)
