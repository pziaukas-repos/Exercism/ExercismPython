import math


def cipher_text(text_plain: str) -> str:
    text_filtered = ''.join(x.lower() for x in text_plain if x.isalnum())
    line_length = int(math.ceil(math.sqrt(len(text_filtered))))
    if line_length == 0:
        return ''

    lines = [text_filtered[k:k+line_length] for k in range(0, len(text_filtered), line_length)]
    lines[-1] = lines[-1].ljust(line_length)
    lines_transposed = [''.join(x) for x in zip(*lines)]

    return ' '.join(lines_transposed)
