from collections import Counter
import re

WORDS = re.compile("[a-z0-9]+(['][a-z]+)?")


def count_words(text: str) -> Counter:
    return Counter(word.group(0) for word in WORDS.finditer(text.lower()))
