class SpaceAge(object):
    def __init__(self, seconds: int):
        self.seconds = seconds

        period_map = {'mercury': 0.2408467, 'venus': 0.61519726, 'earth': 1.0, 'mars': 1.8808158,
                      'jupiter': 11.862615, 'saturn': 29.447498, 'uranus': 84.016846, 'neptune': 164.79132}

        for planet, x in period_map.items():
            setattr(self, f'on_{planet}', lambda x=x: round(seconds / 31557600 / x, 2))
