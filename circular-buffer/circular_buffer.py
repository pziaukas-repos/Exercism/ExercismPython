from typing import Optional


class BufferFullException(Exception):
    pass


class BufferEmptyException(Exception):
    pass


Element = Optional[str]


class CircularBuffer:
    def __init__(self, capacity: int) -> None:
        self.buffer = [None] * capacity
        self.read_index, self.write_index = 0, 0

    def read(self) -> Element:
        if self.buffer[self.read_index] is None:
            raise BufferEmptyException('Empty!')
        data, self.buffer[self.read_index] = self.buffer[self.read_index], None
        self.read_index = (self.read_index + 1) % len(self.buffer)
        return data

    def write(self, data: Element) -> None:
        if self.buffer[self.write_index] is not None:
            raise BufferFullException('Full!')
        self.buffer[self.write_index] = data
        self.write_index = (self.write_index + 1) % len(self.buffer)

    def overwrite(self, data: Element) -> None:
        if self.buffer[self.write_index]:
            self.read()
        self.write(data)

    def clear(self) -> None:
        self.__init__(len(self.buffer))
