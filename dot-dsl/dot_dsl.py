from typing import Dict, List

Attributes = Dict[str, str]

NODE, EDGE, ATTR = range(3)


class Node:
    def __init__(self, name: str, attrs: Attributes) -> None:
        self.name = name
        self.attrs = attrs

    def __eq__(self, other: 'Node') -> bool:
        return self.__dict__ == other.__dict__


class Edge:
    def __init__(self, src: str, dst: str, attrs: Attributes) -> None:
        self.src = src
        self.dst = dst
        self.attrs = attrs

    def __eq__(self, other: 'Edge') -> bool:
        return self.__dict__ == other.__dict__


class Graph(object):
    def __init__(self, data: List[tuple] = None):
        if not all(len(x) > 1 for x in data or []):
            raise TypeError('Malformed graph!')

        self.nodes = []
        self.edges = []
        self.attrs = {}

        for entity, *arguments in data or []:
            if entity == NODE and len(arguments) == 2:
                self.nodes.append(Node(*arguments))
            elif entity == EDGE and len(arguments) == 3:
                self.edges.append(Edge(*arguments))
            elif entity == ATTR and len(arguments) == 2:
                key, value = arguments
                self.attrs[key] = value
            else:
                raise ValueError('Malformed data!')
