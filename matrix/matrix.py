from typing import List

Row = Column = List[int]


class Matrix:
    def __init__(self, matrix_string: str) -> None:
        self.matrix = [[int(x) for x in row.split()] for row in matrix_string.splitlines()]
        self.matrix_t = list(map(list, zip(*self.matrix)))

    def row(self, index: int) -> Row:
        return self.matrix[index - 1].copy()

    def column(self, index: int) -> Column:
        return self.matrix_t[index - 1].copy()
