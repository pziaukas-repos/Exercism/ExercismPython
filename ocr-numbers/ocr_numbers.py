from typing import Generator, List


def generate_cuts(grid: List[str]) -> Generator[str, None, None]:
    width = -(-max(map(len, grid[0:4])) // 3)
    for n in range(width):
        yield ''.join(grid[i][j] for i in range(4) for j in range(3*n, 3*n+3))


GRID = [" _     _  _     _  _  _  _  _ ",
        "| |  | _| _||_||_ |_   ||_||_|",
        "|_|  ||_  _|  | _||_|  ||_| _|",
        "                              "]
OCR = {fingerprint: str(n) for n, fingerprint in enumerate(generate_cuts(GRID))}


def convert(input_grid: List[str]) -> str:
    try:
        output = ''.join(OCR.get(fingerprint, '?') for fingerprint in generate_cuts(input_grid))
    except IndexError:
        raise ValueError('Bad grid size!')
    if len(input_grid) == 4:
        return output
    else:
        return f'{output},{convert(input_grid[4:])}'
