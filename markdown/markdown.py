import re


def parse(markdown):
    markdown = re.sub(r'__(.+?)__', r'<strong>\1</strong>', markdown)
    markdown = re.sub(r'_(.+?)_', r'<em>\1</em>', markdown)

    for h in range(6, 0, -1):
        markdown = re.sub(rf"^{'#' * h} (.+)$", rf'<h{h}>\1</h{h}>', markdown, flags=re.M)
    markdown = re.sub(r'^\* (.+)$', r'<li>\1</li>', markdown, flags=re.M)
    markdown = re.sub(r'(<li>.*</li>)', r'<ul>\1</ul>', markdown, flags=re.S)
    markdown = re.sub(r'^(?!<[hlu])(.+)$', r'<p>\1</p>', markdown, flags=re.M)

    markdown = ''.join(markdown.split('\n'))
    return markdown
