from collections import defaultdict
from typing import List

Children = List[str]


class School:
    def __init__(self) -> None:
        self.journal = defaultdict(list)

    def add_student(self, name: str, grade: int) -> None:
        self.journal[grade].append(name)

    def grade(self, n: int) -> Children:
        return sorted(self.journal[n])

    def roster(self) -> Children:
        result = []
        for n in sorted(self.journal.keys()):
            result += self.grade(n)
        return result
