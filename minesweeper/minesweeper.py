from collections import Counter
from typing import List


def annotate(board_array: List[str]) -> List[str]:
    if len(set(map(len, board_array))) > 1:
        raise ValueError('This is not a board!')

    # phase 1 - scan the board for mines
    mine_neighbors = Counter()
    for x, row in enumerate(board_array):
        for y, value in enumerate(row):
            if value == '*':
                mine_neighbors.update((x + dx, y + dy) for dx in [-1, 0, 1] for dy in [-1, 0, 1])
            elif value == ' ':
                pass
            else:
                raise ValueError('Unrecognized symbol!')

    # phase 2 - update the board numbers
    for x, row in enumerate(board_array):
        row_list = list(row)
        for y, value in enumerate(row_list):
            if value == ' ' and mine_neighbors[(x, y)]:
                row_list[y] = str(mine_neighbors[(x, y)])
        board_array[x] = ''.join(row_list)

    return board_array
