from typing import Optional, Tuple


def handle_error_by_throwing_exception() -> None:
    raise NotImplementedError('Handling an error!')


def handle_error_by_returning_none(input_data: str) -> Optional[int]:
    try:
        return int(input_data)
    except ValueError:
        return None


def handle_error_by_returning_tuple(input_data: str) -> Tuple[bool, Optional[int]]:
    output_data = handle_error_by_returning_none(input_data)
    return output_data is not None, output_data


def filelike_objects_are_closed_on_exception(filelike_object) -> None:
    with filelike_object as handler:
        handler.do_something()
