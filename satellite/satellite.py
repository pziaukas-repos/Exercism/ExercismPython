from typing import Dict, List, Union

Traversal = List[str]
Tree = Dict[str, Union[str, 'Tree']]


def tree_from_traversals(preorder: Traversal, inorder: Traversal) -> Tree:
    if sorted(preorder) != sorted(inorder):
        raise ValueError('Different elements in traversals!')
    if len(preorder) != len(set(preorder)):
        raise ValueError('Duplicate elements in traversals!')

    return build_tree(preorder, inorder)


def build_tree(preorder: Traversal, inorder: Traversal) -> Tree:
    if len(preorder) == 0:
        return {}

    value = preorder[0]
    idx = inorder.index(value)
    return {
        'v': value,
        'l': tree_from_traversals(preorder[1:idx + 1], inorder[:idx]),
        'r': tree_from_traversals(preorder[idx + 1:], inorder[idx + 1:])
    }
