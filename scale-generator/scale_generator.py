from itertools import accumulate
from typing import List


SHARPS = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']
FLATS = ['A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab']
OFFSETS = {'m': 1, 'M': 2, 'A': 3}


class Scale(object):
    def __init__(self, note: str) -> None:
        self.notes = FLATS if 'b' in note or note in 'dFg' else SHARPS
        self.note_proper = note.title()

    def interval(self, steps: str) -> List[str]:
        start_index = self.notes.index(self.note_proper)
        pitches = [self.notes[(start_index + n) % 12] for n in accumulate(OFFSETS[i] for i in steps)]

        if self.note_proper != pitches.pop():
            raise ValueError('Broken interval!')

        return [self.note_proper] + pitches

    def chromatic(self) -> List[str]:
        return self.interval('m' * 12)
