from typing import Dict, List, Union


class SgfTree(object):
    def __init__(self, properties=None, children=None):
        self.properties = properties or {}
        self.children = children or []

    def __eq__(self, other):
        if not isinstance(other, SgfTree):
            return False
        for k, v in self.properties.items():
            if k not in other.properties:
                return False
            if other.properties[k] != v:
                return False
        for k in other.properties.keys():
            if k not in self.properties:
                return False
        if len(self.children) != len(other.children):
            return False
        for a, b in zip(self.children, other.children):
            if a != b:
                return False
        return True


def cut(feed: str, limits: str = '()') -> List[slice]:
    start, end = limits
    slices = []
    stack = []
    marks = enumerate(feed)
    for k, x in marks:
        if x == start:
            stack.append(k)
        elif x == end:
            if len(stack) == 1:
                slices.append(slice(stack.pop(), k + 1))
            elif len(stack) > 1:
                stack.pop()
        elif x == '\\':
            next(marks)
    return slices


def propertize(feed: str) -> Dict[str, List[str]]:
    value_slices = cut(feed, '[]')
    key = feed[:value_slices[0].start]
    if key != key.upper():
        raise ValueError
    return {key: [feed[value_slice][1:-1].replace('\\', '').replace('\t', ' ') for value_slice in value_slices]}


def parse(feed: str) -> Union[SgfTree, List[SgfTree]]:
    try:
        tree_slices = cut(feed)
        if len(tree_slices) == 1:
            if feed == '(;)':
                return SgfTree()
            property_feed = feed.split(';', 2)[1].rstrip('(')
            remainder = feed[len(property_feed) + 2:-1]
            if remainder == '':
                return SgfTree(propertize(property_feed))
            elif remainder[0] == ';':
                return SgfTree(propertize(property_feed), [parse(f'({remainder})')])
            return SgfTree(propertize(property_feed), parse(remainder))
        elif len(tree_slices) > 1:
            return [parse(feed[tree_slice]) for tree_slice in tree_slices]
        else:
            raise ValueError
    except (IndexError, ValueError):
        raise ValueError('Tree is not valid!')

    pass
