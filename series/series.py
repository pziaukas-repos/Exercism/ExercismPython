from typing import List


def slices(series: str, length: int) -> List[str]:
    if not 0 < length <= len(series):
        raise ValueError('The length must be between 1 and the length of the series!')

    return [series[k:k + length] for k in range(len(series) - length + 1)]
