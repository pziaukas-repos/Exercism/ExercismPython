from dataclasses import astuple, dataclass
from itertools import permutations


@dataclass(frozen=True)
class ConstantColor:
    _label = 'color'
    RED: str = 'Red'
    BLUE: str = 'Blue'
    IVORY: str = 'Ivory'
    YELLOW: str = 'Yellow'
    GREEN: str = 'Green'


@dataclass(frozen=True)
class ConstantNationality:
    _label = 'nationality'
    NORWEGIAN: str = 'Norwegian'
    SPANIARD: str = 'Spaniard'
    UKRAINIAN: str = 'Ukrainian'
    JAPANESE: str = 'Japanese'
    ENGLISHMAN: str = 'Englishman'


@dataclass(frozen=True)
class ConstantPet:
    _label = 'pet'
    DOG: str = 'Dog'
    SNAIL: str = 'Snail'
    FOX: str = 'Fox'
    HORSE: str = 'Horse'
    ZEBRA: str = 'Zebra'


@dataclass(frozen=True)
class ConstantBeverage:
    _label = 'beverage'
    COFFEE: str = 'Coffee'
    TEA: str = 'Tea'
    MILK: str = 'Milk'
    ORANGE_JUICE: str = 'Orange Juice'
    WATER: str = 'Water'


@dataclass(frozen=True)
class ConstantCigarette:
    _label = 'cigarette'
    KOOL: str = 'Kool'
    OLD_GOLD: str = 'Old Gold'
    CHESTERFIELD: str = 'Chesterfield'
    LUCKY_STRIKE: str = 'Lucky Strike'
    PARLIAMENT: str = 'Parliament'


COLOR = ConstantColor()
NATIONALITY = ConstantNationality()
PET = ConstantPet()
BEVERAGE = ConstantBeverage()
CIGARETTE = ConstantCigarette()


@dataclass
class House:
    number: int
    color: str = None
    nationality: str = None
    pet: str = None
    beverage: str = None
    cigarette: str = None


class HouseBlock:
    def __init__(self):
        self.houses = [House(k) for k in range(5)]

    def house(self, value):
        return next(house for house in self.houses if value in astuple(house))

    def fit(self, descriptor, sequence):
        values = astuple(descriptor)
        for number, house in zip(sequence, self.houses):
            setattr(house, descriptor._label, values[number])

        if isinstance(descriptor, ConstantColor):
            return self.house(COLOR.GREEN).number - 1 == self.house(COLOR.IVORY).number and \
                   self.house(1) == self.house(COLOR.BLUE)
        if isinstance(descriptor, ConstantNationality):
            return self.house(COLOR.RED) == self.house(NATIONALITY.ENGLISHMAN) and \
                   self.house(0) == self.house(NATIONALITY.NORWEGIAN)
        if isinstance(descriptor, ConstantPet):
            return self.house(NATIONALITY.SPANIARD) == self.house(PET.DOG)
        if isinstance(descriptor, ConstantBeverage):
            return self.house(COLOR.GREEN) == self.house(BEVERAGE.COFFEE) and \
                   self.house(NATIONALITY.UKRAINIAN) == self.house(BEVERAGE.TEA) and \
                   self.house(2) == self.house(BEVERAGE.MILK)
        if isinstance(descriptor, ConstantCigarette):
            return self.house(CIGARETTE.OLD_GOLD) == self.house(PET.SNAIL) and \
                   self.house(COLOR.YELLOW) == self.house(CIGARETTE.KOOL) and \
                   abs(self.house(CIGARETTE.CHESTERFIELD).number - self.house(PET.FOX).number) == 1 and \
                   abs(self.house(CIGARETTE.KOOL).number - self.house(PET.HORSE).number) == 1 and \
                   self.house(CIGARETTE.LUCKY_STRIKE) == self.house(BEVERAGE.ORANGE_JUICE) and \
                   self.house(NATIONALITY.JAPANESE) == self.house(CIGARETTE.PARLIAMENT)


class HouseBlockSolution(HouseBlock):
    def __init__(self):
        super(HouseBlockSolution, self).__init__()

        for sequence_color in permutations(range(5)):
            if not self.fit(COLOR, sequence_color):
                continue
            for sequence_nationality in permutations(range(5)):
                if not self.fit(NATIONALITY, sequence_nationality):
                    continue
                for sequence_pet in permutations(range(5)):
                    if not self.fit(PET, sequence_pet):
                        continue
                    for sequence_beverage in permutations(range(5)):
                        if not self.fit(BEVERAGE, sequence_beverage):
                            continue
                        for sequence_cigarette in permutations(range(5)):
                            if self.fit(CIGARETTE, sequence_cigarette):
                                return


solution = HouseBlockSolution()


def drinks_water():
    return solution.house(BEVERAGE.WATER).nationality


def owns_zebra():
    return solution.house(PET.ZEBRA).nationality
