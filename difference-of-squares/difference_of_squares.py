def square_of_sum(count: int) -> int:
    return (count * (count + 1) // 2) ** 2


def sum_of_squares(count: int) -> int:
    return count * (count + 1) * (2 * count + 1) // 6


def difference_of_squares(count: int) -> int:
    return square_of_sum(count) - sum_of_squares(count)
