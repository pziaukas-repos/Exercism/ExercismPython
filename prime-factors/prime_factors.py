def factors(natural_number: int) -> list:
    prime_factors = []
    prime = 2
    while prime * prime <= natural_number:
        if natural_number % prime:
            prime += 1
        else:
            natural_number //= prime
            prime_factors.append(prime)
    if natural_number > 1:
        prime_factors.append(natural_number)
    return prime_factors
