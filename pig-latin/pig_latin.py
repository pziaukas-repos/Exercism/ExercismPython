def translate(text: str) -> str:
    words = text.split()
    pig_words = []
    for word in words:
        if word[0] in 'aeiou' or (word[0] in 'xy' and word[1] not in 'aeiou'):
            pig_words.append(word + 'ay')
        elif word[0:3] in ['thr', 'sch', 'squ']:
            pig_words.append(word[3:] + word[0:3] + 'ay')
        elif word[0:2] in ['rh', 'th', 'qu', 'ch']:
            pig_words.append(word[2:] + word[0:2] + 'ay')
        else:
            pig_words.append(word[1:] + word[0] + 'ay')
    return ' '.join(pig_words)
