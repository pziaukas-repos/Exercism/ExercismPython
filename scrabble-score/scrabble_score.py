value_by_letter = {}
value_by_letter.update(dict.fromkeys(['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'], 1))
value_by_letter.update(dict.fromkeys(['D', 'G'], 2))
value_by_letter.update(dict.fromkeys(['B', 'C', 'M', 'P'], 3))
value_by_letter.update(dict.fromkeys(['F', 'H', 'V', 'W', 'Y'], 4))
value_by_letter.update(dict.fromkeys(['K'], 5))
value_by_letter.update(dict.fromkeys(['J', 'X'], 8))
value_by_letter.update(dict.fromkeys(['Q', 'Z'], 10))


def score(word: str) -> int:
    return sum(value_by_letter[letter.upper()] for letter in word)
