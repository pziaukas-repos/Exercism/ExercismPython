from typing import List


def find(list_of_numbers: List[int], number: int) -> int:
    low = 0
    high = len(list_of_numbers) - 1
    while low <= high:
        mid = (low + high) // 2
        if list_of_numbers[mid] > number:
            high = mid - 1
        elif list_of_numbers[mid] < number:
            low = mid + 1
        else:
            return mid
    raise ValueError('Value not found!')
