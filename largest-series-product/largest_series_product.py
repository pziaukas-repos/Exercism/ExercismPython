from typing import List


def product(numbers: List[int]) -> int:
    p = 1
    for k in numbers:
        p *= k
    return p


def largest_product(series: str, size: int) -> int:
    if 0 < size <= len(series):
        return max(product([int(x) for x in series[k:k + size]]) for k in range(len(series) - size + 1))
    elif size == 0:
        return 1
    else:
        raise ValueError('Size is out of bounds!')
