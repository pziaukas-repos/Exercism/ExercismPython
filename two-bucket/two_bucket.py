class Bucket:
    def __init__(self, label: str, size: int):
        self.label = label
        self.size = size
        self.level = 0

    def fill(self, other: 'Bucket' = None) -> None:
        if other is None:
            self.level = self.size
        else:
            amount = min(self.size - self.level, other.level)
            self.level += amount
            other.level -= amount

    def empty(self) -> None:
        self.level = 0

    @property
    def is_full(self) -> bool:
        return self.level == self.size

    @property
    def is_empty(self) -> bool:
        return self.level == 0


def measure(one_cap, two_cap, goal, first):
    steps = 0
    this_bucket = Bucket('one', one_cap)
    that_bucket = Bucket('two', two_cap)
    if first != this_bucket.label:
        this_bucket, that_bucket = that_bucket, this_bucket

    while goal not in [this_bucket.level, that_bucket.level]:
        steps += 1
        if this_bucket.is_empty:
            this_bucket.fill()
        elif that_bucket.size == goal:
            that_bucket.fill()
        elif that_bucket.is_full:
            that_bucket.empty()
        else:
            that_bucket.fill(this_bucket)

    if this_bucket.level != goal:
        this_bucket, that_bucket = that_bucket, this_bucket
    return steps, this_bucket.label, that_bucket.level
