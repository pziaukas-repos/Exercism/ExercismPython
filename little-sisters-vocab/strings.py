"""Little Sister's Vocabulary solution"""
import string


def add_prefix(word: str, prefix: str) -> str:
    """Add the given prefix to the word."""
    return f'{prefix}{word}'


def add_suffix(word: str, suffix: str) -> str:
    """Add the given suffix to the word."""
    return f'{word}{suffix}'


def remove_suffix(word: str, suffix: str) -> str:
    """Remove the given suffix from the word."""
    if word.endswith(suffix):
        return word[:-len(suffix)]
    raise ValueError('Suffix not found!')


def add_prefix_un(word: str) -> str:
    """

    :param word: str of a root word
    :return:  str of root word with un prefix

    This function takes `word` as a parameter and
    returns a new word with an 'un' prefix.
    """

    return add_prefix(word, 'un')


def make_word_groups(vocab_words: list[str]) -> str:
    """

    :param vocab_words: list of vocabulary words with a prefix.
    :return: str of prefix followed by vocabulary words with
             prefix applied, separated by ' :: '.

    This function takes a `vocab_words` list and returns a string
    with the prefix  and the words with prefix applied, separated
     by ' :: '.
    """

    prefix, *words = vocab_words
    return ' :: '.join([prefix] + [add_prefix(word, prefix) for word in words])


def remove_suffix_ness(word: str) -> str:
    """

    :param word: str of word to remove suffix from.
    :return: str of word with suffix removed & spelling adjusted.

    This function takes in a word and returns the base word with `ness` removed.
    """

    word_without_ness = remove_suffix(word, 'ness')
    try:
        # Replace 'i' with 'y' if needed
        return add_suffix(remove_suffix(word_without_ness, 'i'), 'y')
    except ValueError:
        # Return the unadjusted word otherwise
        return word_without_ness


def noun_to_verb(sentence: str, index: int) -> str:
    """

    :param sentence: str that uses the word in sentence
    :param index:  index of the word to remove and transform
    :return:  str word that changes the extracted adjective to a verb.

    A function takes a `sentence` using the
    vocabulary word, and the `index` of the word once that sentence
    is split apart.  The function should return the extracted
    adjective as a verb.
    """

    word = sentence.split()[index].strip(string.punctuation)
    return add_suffix(word, 'en')
