DEFICIENT = 'deficient'
PERFECT = 'perfect'
ABUNDANT = 'abundant'


def aliquot_sum(n: int) -> int:
    result = 0
    k = 1
    while k * k <= n:
        if n % k == 0:
            result += k
            if k * k != n:
                result += n // k
        k += 1
    return result - n


def classify(n: int) -> str:
    if n < 1:
        raise ValueError('Only natural numbers are allowed!')

    m = aliquot_sum(n)
    if n < m:
        return ABUNDANT
    elif n > m:
        return DEFICIENT
    return PERFECT
