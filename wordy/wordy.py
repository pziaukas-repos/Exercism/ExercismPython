import re


def answer(question):
    operations = {'plus': '+', 'minus': '-', 'multiplied by': '*', 'divided by': '//'}
    pattern = re.compile(fr"What is (-?\d+) ({'|'.join(x for x in operations.keys())}) (-?\d+)\s?(.*)")
    try:
        first, op, second, remainder = re.search(pattern, question).groups()
    except AttributeError:
        raise ValueError('Unknown question asked!')

    response = eval(f'{first} {operations[op]} {second}')
    return response if remainder == '?' else answer(f'What is {response} {remainder}')
