# -*- coding: utf-8 -*-
from bisect import insort
from dataclasses import dataclass
from datetime import datetime
from typing import List, Tuple


@dataclass(order=True)
class LedgerEntry:
    date: datetime
    change: int
    description: str


@dataclass
class Locale:
    abbreviation: str
    date: str
    description: str
    change: str

    @staticmethod
    def format_date(date: datetime) -> str:
        return str(date)

    @staticmethod
    def format_description(description: str, width: int) -> str:
        return f'{description if len(description) <= width else description[:width - 3] + "..."}'

    @staticmethod
    def format_change(change: int) -> str:
        return f'{change / 100.0:,.2f}'


class LocaleEnglish(Locale):
    def __init__(self) -> None:
        super(LocaleEnglish, self).__init__('en_US', 'Date', 'Description', 'Change')

    @staticmethod
    def format_date(date: datetime) -> str:
        return date.strftime('%m/%d/%Y')

    @staticmethod
    def format_change(change: int, currency_code: str = '') -> str:
        result = f'{currency_code}{Locale.format_change(abs(change))}'
        return f' {result} ' if change >= 0 else f'({result})'


class LocaleDutch(Locale):
    def __init__(self) -> None:
        super(LocaleDutch, self).__init__('nl_NL', 'Datum', 'Omschrijving', 'Verandering')

    @staticmethod
    def format_date(date: datetime) -> str:
        return date.strftime('%d-%m-%Y')

    @staticmethod
    def format_change(change: int, currency_code: str = '') -> str:
        return f'{currency_code} {Locale.format_change(change).translate(str.maketrans(",.", ".,"))} '


LOCALES = {x.abbreviation: x for x in [LocaleEnglish(), LocaleDutch()]}
CURRENCY_CODES = {'USD': '$', 'EUR': '€'}


class Ledger:
    def __init__(self, currency: str, locale_abbreviation: str, widths: Tuple[int, int, int]) -> None:
        self.locale = LOCALES[locale_abbreviation]
        self.currency_code = CURRENCY_CODES[currency]
        self.w_date, self.w_description, self.w_change = widths
        self.entries = []

    def add(self, entry: LedgerEntry) -> None:
        insort(self.entries, entry)

    def __str__(self) -> str:
        template = f'{{:<{self.w_date}}} | {{:<{self.w_description}}} | {{:>{self.w_change}}}'
        header = self.locale.date, self.locale.description, self.locale.change.ljust(self.w_change)

        rows = [(self.locale.format_date(entry.date),
                 self.locale.format_description(entry.description, self.w_description),
                 self.locale.format_change(entry.change, self.currency_code)) for entry in self.entries]

        return '\n'.join([template.format(*row) for row in [header] + rows])


def create_entry(date: str, description: str, change: int) -> LedgerEntry:
    return LedgerEntry(datetime.strptime(date, '%Y-%m-%d'), change, description)


def format_entries(currency: str, locale_abbreviation: str, entries: List[LedgerEntry]) -> str:
    ledger = Ledger(currency, locale_abbreviation, (10, 25, 13))
    for entry in entries:
        ledger.add(entry)
    return str(ledger)
