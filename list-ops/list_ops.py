from typing import Any, Callable, List


def append(xs: List, ys: List) -> List:
    return concat([xs, ys])


def concat(lists: List[List]) -> List:
    return [x for xs in lists for x in xs]


def filter(fun: Callable, xs: List) -> List:
    return [x for x in xs if fun(x)]


def length(xs: List) -> int:
    return sum(1 for _ in xs)


def map(fun: Callable, xs: List) -> List:
    return [fun(x) for x in xs]


def foldl(fun: Callable, xs: List, acc: Any) -> Any:
    res = acc
    for x in xs:
        res = fun(res, x)
    return res


def foldr(fun: Callable, xs: List, acc: Any) -> Any:
    res = acc
    for x in reverse(xs):
        res = fun(x, res)
    return res


def reverse(xs: List) -> List:
    return xs[::-1]
