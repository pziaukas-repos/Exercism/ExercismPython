from typing import List


def sum_of_multiples(limit: int, multiples: List[int]) -> int:
    nums = set()
    for x in multiples:
        if not x:
            continue
        nums.update(range(x, limit, x))
    return sum(nums)
