class Frame:
    def __init__(self) -> None:
        self.rolls = []

    def __int__(self) -> int:
        if not self.finished:
            raise IndexError('Unfinished frame cannot be scored!')
        return sum(self.rolls)

    def roll(self, pins: int) -> None:
        if self.finished:
            raise IndexError('Finished frame cannot be scored!')
        if not 0 <= pins <= 10:
            raise ValueError('Impossible pins in a roll!')
        if sum(self.rolls) % 10 and self.rolls[-1] + pins > 10:
            raise ValueError('Impossible pins in a frame!')
        self.rolls.append(pins)

    @property
    def finished(self) -> bool:
        return len(self.rolls) == 3 if sum(self.rolls[:2]) >= 10 else len(self.rolls) == 2


class BowlingGame:
    def __init__(self) -> None:
        self.frames = []
        self.upcoming_frame = True

    def roll(self, pins: int) -> None:
        if self.upcoming_frame:
            self.frames.append(Frame())
        self.upcoming_frame = len(self.frames) < 10 and (True if pins == 10 else not self.upcoming_frame)

        frames_unfinished = [frame for frame in self.frames if not frame.finished]
        if not frames_unfinished:
            raise IndexError('Cannot roll in a complete game!')
        for frame in frames_unfinished:
            frame.roll(pins)

    def score(self) -> int:
        if self.upcoming_frame:
            raise IndexError('Unfinished game cannot be scored!')
        return sum(int(frame) for frame in self.frames)
