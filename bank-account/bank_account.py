import threading


class BankAccount(object):
    def __init__(self):
        self.lock = threading.RLock()
        self._balance = None

    @property
    def balance(self):
        if self._balance is None:
            raise ValueError('Account is not open!')
        return self._balance

    def get_balance(self):
        return self.balance

    def open(self):
        if self._balance is not None:
            raise ValueError('Already open!')
        self._balance = 0

    def close(self):
        if self._balance is None:
            raise ValueError('Already closed!')
        self._balance = None

    def deposit(self, amount):
        with self.lock:
            if amount <= 0:
                raise ValueError('Incorrect amount!')
            self._balance = self.balance + amount

    def withdraw(self, amount):
        with self.lock:
            if not 0 < amount <= self.balance:
                raise ValueError('Incorrect amount!')
            self._balance = self.balance - amount
