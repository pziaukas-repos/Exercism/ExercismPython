from typing import Iterator


def generate_zigzag(rails_count: int, depth: int = None) -> Iterator[int]:
    def zigzag():
        while True:
            yield from range(rails_count - 1)
            yield from range(rails_count - 1, 0, -1)
    generator = zigzag()
    if depth is None:
        yield from generator
    else:
        for _ in range(depth):
            yield next(generator)


def encode(message: str, rails_count: int) -> str:
    rails = [''] * rails_count
    for letter, n in zip(message, generate_zigzag(rails_count)):
        rails[n] += letter
    return ''.join(rails)


def decode(encoded_message: str, rails_count: int) -> str:
    rails = [[] for _ in range(rails_count)]
    for letter, n in zip(encoded_message, sorted(generate_zigzag(rails_count, len(encoded_message)))):
        rails[n] = [letter] + rails[n]
    message = ''
    for n in generate_zigzag(rails_count, len(encoded_message)):
        message += rails[n].pop()
    return message
