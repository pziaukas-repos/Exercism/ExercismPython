from secrets import choice


def private_key(p: int) -> int:
    return choice(range(2, p))


def public_key(p: int, g: int, private: int) -> int:
    return g ** private % p


def secret(p: int, public: int, private: int) -> int:
    return public ** private % p
