def is_paired(input_string: str) -> bool:
    pairs = ['()', '[]', '{}']
    openers, closers = zip(*pairs)
    stack = []
    for x in input_string:
        if x in openers:
            stack.append(x)
        if x in closers:
            if not stack or f'{stack.pop()}{x}' not in pairs:
                return False
    return True if not stack else False
