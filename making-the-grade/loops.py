"""Making the Grade solution."""


def round_scores(student_scores: list[float]) -> list[int]:
    """Convert decimal grades to integer grades.
    :param student_scores: list of student exam scores as float or int.
    :return: list of student scores *rounded* to nearest integer value.
    """

    return [round(score) for score in student_scores]


def count_failed_students(student_scores: list[int]) -> int:
    """Count the number of failed students.
    :param student_scores: list of integer student scores.
    :return: integer count of student scores at or below 40.
    """

    return sum(1 for score in student_scores if score <= 40)


def above_threshold(student_scores: list[int], threshold: int) -> list[int]:
    """Get the list of student scores above the given threshold.
    :param student_scores: list of integer scores
    :param threshold :  integer
    :return: list of integer scores that are at or above the "best" threshold.
    """

    return [score for score in student_scores if score >= threshold]


def letter_grades(highest: int) -> list[int]:
    """Calculate lower thresholds of letter grades.
    :param highest: integer of highest exam score.
    :return: list of integer score thresholds for each F-A letter grades.
    """

    span = (highest - 40) // 4
    return [41 + n * span for n in range(4)]


def student_ranking(student_scores: list[int], student_names: list[str]) -> list[str]:
    """Generate the ranking of students.
     :param student_scores: list of scores in descending order.
     :param student_names: list of names in descending order by exam score.
     :return: list of strings in format ["<rank>. <student name>: <score>"].
     """

    return [f'{rank}. {name}: {score}' for rank, (name, score) in enumerate(zip(student_names, student_scores), 1)]


def perfect_score(student_info: list[list]) -> list:
    """Looks for perfect scores.
    :param student_info: list of [<student name>, <score>] lists
    :return: first `[<student name>, 100]` or `[]` if no student score of 100 is found.
    """

    for name, score in student_info:
        if score == 100:
            return [name, 100]
    return []
