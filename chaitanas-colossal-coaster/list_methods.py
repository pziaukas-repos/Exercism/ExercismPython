"""Chaitana's Colossal Coaster solution."""

Queue = list[str]


def add_me_to_the_queue(express_queue: Queue, normal_queue: Queue, ticket_type: int, person_name: str) -> Queue:
    """Add me to the queue.

    :param express_queue: list - names in the Fast-track queue.
    :param normal_queue:  list - names in the normal queue.
    :param ticket_type:  int - type of ticket. 1 = express, 0 = normal.
    :param person_name: str - name of person to add to a queue.
    :return: list - the (updated) queue the name was added to.
    """
    queue_by_type = {0: normal_queue, 1: express_queue}
    return queue_by_type[ticket_type] + [person_name]


def find_my_friend(queue: Queue, friend_name: str) -> int:
    """Where are my friends?

    :param queue: list - names in the queue.
    :param friend_name: str - name of friend to find.
    :return: int - index at which the friends name was found.
    """
    return queue.index(friend_name)


def add_me_with_my_friends(queue: Queue, index: int, person_name: str) -> Queue:
    """Can I please join them?

    :param queue: list - names in the queue.
    :param index: int - the index at which to add the new name.
    :param person_name: str - the name to add.
    :return: list - queue updated with new name.
    """
    queue.insert(index, person_name)
    return queue


def remove_the_mean_person(queue: Queue, person_name: str) -> Queue:
    """Mean person in the queue.

    :param queue: list - names in the queue.
    :param person_name: str - name of mean person.
    :return:  list - queue update with the mean persons name removed.
    """
    queue.remove(person_name)
    return queue


def how_many_namefellows(queue: Queue, person_name: str) -> int:
    """Namefellows.

    :param queue: list - names in the queue.
    :param person_name: str - name you wish to count or track.
    :return:  int - the number of times the name appears in the queue.
    """
    return queue.count(person_name)


def remove_the_last_person(queue: Queue) -> str:
    """Remove the last person.

    :param queue: list - names in the queue.
    :return: str - name that has been removed from the end of the queue.
    """
    return queue.pop()


def sorted_names(queue: Queue) -> Queue:
    """Sort the Queue List.

    :param queue: list - names in the queue.
    :return: list - copy of the queue in alphabetical order.
    """
    return sorted(queue)
