from json import dumps
from typing import Dict, Iterator, List


class Tree:
    def __init__(self, label: str, children: List['Tree'] = None) -> None:
        self.label = label
        self.children = children or []

    def __dict__(self) -> Dict[str, List[dict]]:
        return {self.label: [c.__dict__() for c in sorted(self.children)]}

    def __str__(self, indent: int = None) -> str:
        return dumps(self.__dict__(), indent=indent)

    def __lt__(self, other: 'Tree') -> bool:
        return self.label < other.label

    def __eq__(self, other: 'Tree') -> bool:
        return self.__dict__() == other.__dict__()

    def __iter__(self) -> Iterator[str]:
        yield self.label
        for child in self.children:
            yield from child

    def copy(self) -> 'Tree':
        return Tree(self.label, [x.copy() for x in self.children])

    def add(self, other: 'Tree') -> 'Tree':
        tree = self.copy()
        tree.children.append(other)
        return tree

    def remove(self, node: str) -> 'Tree':
        tree = self.copy()
        for child in list(tree.children):
            tree.children.remove(child)
            if child.label == node:
                break
            tree.children.append(child.remove(node))
        return tree

    def from_pov(self, from_node: str) -> 'Tree':
        stack = [self]
        visited = set()
        while stack:
            tree = stack.pop(0)
            if tree.label in visited:
                continue
            visited.add(tree.label)
            if from_node == tree.label:
                return tree
            for child in tree.children:
                stack.append(child.add(tree.remove(child.label)))
        raise ValueError('Invalid node!')

    def path_to(self, from_node: str, to_node: str) -> List[str]:
        tree = self.from_pov(from_node)
        stack = tree.children
        path = [from_node]
        while path[-1] != to_node:
            try:
                tree = stack.pop()
            except IndexError:
                raise ValueError('No path found!')
            if to_node in tree:
                path.append(tree.label)
                stack = tree.children
        return path
