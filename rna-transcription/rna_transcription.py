def to_rna(dna_strand):
    dna_map = {'G': 'C', 'C': 'G', 'T': 'A', 'A': 'U'}

    try:
        return ''.join([dna_map[x] for x in dna_strand])
    except KeyError:
        raise ValueError('DNA strand is invalid')
