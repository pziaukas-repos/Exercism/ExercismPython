from itertools import cycle
from secrets import choice
import string

ALPHABET = string.ascii_lowercase


class Cipher(object):
    def __init__(self, key: str = None):
        if key is None:
            self.key = ''.join(choice(ALPHABET) for k in range(100))
        elif key.islower() and key.isalpha():
            self.key = key
        else:
            raise ValueError('Key is not lowercase-only!')

    def code(self, text: str, forward: bool) -> str:
        def shift_letter(letter: str, offset: int):
            return ALPHABET[(ALPHABET.index(letter) + offset) % 26]

        plaintext = ''.join(symbol.lower() for symbol in text if symbol.isalpha())
        offsets = cycle([ALPHABET.index(x) if forward else -ALPHABET.index(x) for x in self.key])

        return ''.join(shift_letter(letter, offset) for letter, offset in zip(plaintext, offsets))

    def encode(self, text: str) -> str:
        return self.code(text, forward=True)

    def decode(self, text: str) -> str:
        return self.code(text, forward=False)


class Caesar(Cipher):
    def __init__(self):
        super().__init__('d')
