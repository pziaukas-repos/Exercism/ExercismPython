from heapq import nlargest
from typing import List

Scores = List[int]


def latest(scores: Scores) -> int:
    return scores[-1]


def personal_best(scores: Scores) -> int:
    return max(scores)


def personal_top_three(scores: Scores) -> Scores:
    return nlargest(3, scores)
