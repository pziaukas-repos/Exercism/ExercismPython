from typing import List


def grep(pattern: str, flags: str, files: List[str]) -> str:
    match_base = (lambda p, s: p == s) if 'x' in flags else (lambda p, s: p in s)
    match_case = (lambda p, s: match_base(p.lower(), s.lower())) if 'i' in flags else match_base
    match = (lambda p, s: not match_case(p, s)) if 'v' in flags else match_case

    # sub-selection of the triplet: file name, line number, line
    form = (0,) if 'l' in flags else ((0, 1, 2) if 'n' in flags else (0, 2))[len(files) == 1:]

    names_only = 'l' in flags

    results = []
    for file in files:
        with open(file) as stream:
            for n, line in enumerate(stream.readlines(), 1):
                line = line.rstrip('\n')
                if match(pattern, line):
                    results.append(':'.join((file, str(n), line)[f] for f in form) + '\n')
                    if names_only:
                        break

    return ''.join(results)
