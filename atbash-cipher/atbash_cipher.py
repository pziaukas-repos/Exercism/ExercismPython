import string


def code(text: str) -> str:
    symbols = ''.join(x.lower() for x in text if x.isalnum())
    mirror = str.maketrans(string.ascii_lowercase, string.ascii_lowercase[::-1])
    return symbols.translate(mirror)


def encode(plain_text: str) -> str:
    cipher = code(plain_text)
    return ' '.join([cipher[k:k + 5] for k in range(0, len(cipher), 5)])


def decode(ciphered_text: str) -> str:
    return code(ciphered_text)
