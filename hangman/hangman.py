STATUS_WIN, STATUS_LOSE, STATUS_ONGOING = range(3)


class Hangman:
    def __init__(self, word: str) -> None:
        self.remaining_guesses = 9
        self.answer = word
        self.letters_done = set()
        self.letters_todo = {x for x in word}

    def guess(self, char: str) -> None:
        if self.get_status() != STATUS_ONGOING:
            raise ValueError('The game is over!')
        try:
            self.letters_todo.remove(char)
            self.letters_done.add(char)
        except KeyError:
            self.remaining_guesses -= 1

    def get_masked_word(self) -> str:
        return ''.join(x if x in self.letters_done else '_' for x in self.answer)

    def get_status(self) -> str:
        if len(self.letters_todo):
            return STATUS_LOSE if self.remaining_guesses < 0 else STATUS_ONGOING
        return STATUS_WIN
