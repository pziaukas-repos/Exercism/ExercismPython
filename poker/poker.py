from collections import Counter
from typing import List, Tuple

Hand = str
Rating = Tuple[Tuple[int, ...], Tuple[int, ...]]
RANKS = {r: k + 11 for k, r in enumerate('JQKA')}


def best_hands(hands: List[Hand]) -> List[Hand]:
    best_rating = hand_rating(max(hands, key=hand_rating))
    return [h for h in hands if hand_rating(h) == best_rating]


def hand_rating(hand: Hand) -> Rating:
    cards = [(value_suit[:-1], value_suit[-1]) for value_suit in hand.split()]
    rank_counter = Counter(RANKS.get(card[0]) or int(card[0]) for card in cards)
    score_ranks = [(n, r) for r, n in rank_counter.most_common()]
    score, ranks = zip(*sorted(score_ranks, reverse=True))
    if len(score) == 5:
        if ranks == (14, 5, 4, 3, 2):
            ranks = (5, 4, 3, 2, 1)
        is_flush = len(set(card[1] for card in cards)) == 1
        is_straight = ranks[0] - ranks[4] == 4
        if is_straight:
            score = (5,) if is_flush else (3, 1, 2)
        elif is_flush:
            score = (3, 1, 3)
    return score, ranks
