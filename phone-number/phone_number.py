class Phone(object):
    def __init__(self, phone_number: str):
        noise = str.maketrans('', '', '+-.() ')
        digits = [int(k) for k in phone_number.translate(noise)]
        if len(digits) == 11 and digits[0] == 1:
            digits = digits[1:]
        if len(digits) == 10 and all(digits[k] > 1 for k in [0, 3]):
            self.number = ''.join(str(x) for x in digits)
        else:
            raise ValueError('Format mismatch!')

    @property
    def area_code(self):
        return self.number[0:3]

    @property
    def exchange_code(self):
        return self.number[3:6]

    @property
    def subscriber_number(self):
        return self.number[6:]

    def pretty(self):
        return f'({self.area_code}) {self.exchange_code}-{self.subscriber_number}'
