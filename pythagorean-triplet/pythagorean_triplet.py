from typing import Iterator, Set, Tuple

Triplet = Tuple[int, int, int]


def gcd(a: int, b: int) -> int:
    while b:
        a, b = b, a % b
    return a


def primitive_triplets(number: int) -> Iterator[Triplet]:
    if number % 4:
        raise ValueError('Only multiples of 4 are supported!')

    mn = number // 2
    n = 1
    while n * n < mn:
        if mn % n == 0:
            m = mn // n
            if gcd(m, n) == 1:
                a, b, c = sorted([number, m**2 - n**2, m**2 + n**2])
                yield a, b, c
        n += 1


def triplets_in_range(start: int, end: int) -> Iterator[Triplet]:
    for b in range(4, end + 1, 4):
        for x, y, z in primitive_triplets(b):
            a, b, c = x, y, z
            while a < start:
                a, b, c = (a + x, b + y, c + z)
            while c <= end:
                yield a, b, c
                a, b, c = (a + x, b + y, c + z)


def triplets_with_sum(number: int) -> Set[Triplet]:
    return {t for t in triplets_in_range(1, number // 2) if sum(t) == number}
