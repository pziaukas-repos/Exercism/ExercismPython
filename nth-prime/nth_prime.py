from typing import Iterator


def prime_generator() -> Iterator[int]:
    yield 2
    catalogue = {}
    p = 3
    while True:
        q = catalogue.pop(p, None)
        if q:
            p_next = p + q
            while p_next in catalogue:
                p_next += q
            catalogue[p_next] = q
        else:
            yield p
            catalogue[p * p] = 2 * p
        p += 2


def prime(positive_number: int) -> int:
    if positive_number < 1:
        raise ValueError('Not a natural number!')

    generator = prime_generator()
    for k in range(positive_number):
        prime = next(generator)

    return prime
