class Luhn(object):
    def __init__(self, card_num: str) -> None:
        self.card_num = card_num

    def valid(self) -> bool:
        try:
            series = [int(x) for x in reversed(self.card_num) if x != ' ']
        except ValueError:
            return False

        if len(series) < 2:
            return False
        else:
            odd_sum = sum(a for a in series[::2])
            even_sum = sum(b * 2 if b < 5 else b * 2 - 9 for b in series[1::2])
            return (odd_sum + even_sum) % 10 == 0
