from typing import List


def say_lyrics(verse: int) -> str:
    full_house = 'house that Jack built.'
    subject_actions = [
        ('horse and the hound and the horn', 'belonged to'),
        ('farmer sowing his corn', 'kept'),
        ('rooster that crowed in the morn', 'woke'),
        ('priest all shaven and shorn', 'married'),
        ('man all tattered and torn', 'kissed'),
        ('maiden all forlorn', 'milked'),
        ('cow with the crumpled horn', 'tossed'),
        ('dog', 'worried'),
        ('cat', 'killed'),
        ('rat', 'ate'),
        ('malt', 'lay'),
    ]

    if verse > 1:
        full_house = ' the '.join(f'{who} that {what}' for who, what in subject_actions[-verse+1:]) +\
                     f' in the {full_house}'
    return f'This is the {full_house}'


def recite(start_verse: int, end_verse: int) -> List[str]:
    return [say_lyrics(k) for k in range(start_verse, end_verse + 1)]
