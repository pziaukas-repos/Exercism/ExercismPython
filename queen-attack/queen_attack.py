class Queen:
    def __init__(self, row: int, column: int) -> None:
        if row not in range(8) or column not in range(8):
            raise ValueError('Impossible position!')
        self.row = row
        self.column = column

    def __eq__(self, other: 'Queen') -> bool:
        return self.__dict__ == other.__dict__

    def can_attack(self, another_queen: 'Queen') -> bool:
        if self == another_queen:
            raise ValueError('Same position!')

        return self.row == another_queen.row or \
            self.column == another_queen.column or \
            abs(self.row - another_queen.row) == abs(self.column - another_queen.column)
