from typing import Dict, List, Optional, Set, Tuple

Point = Tuple[int, int]
NONE, WHITE, BLACK = ' ', 'W', 'B'
COMPASS = [(-1, 0), (0, 1), (1, 0), (0, -1)]


class Board:
    """Count territories of each player in a Go game

    Args:
        board (list[str]): A two-dimensional Go board
    """

    def __init__(self, board: List[str]) -> None:
        self.board = board
        self.height = len(board)
        self.width = len(board[0]) if self.height else 0

    def get_value(self, x: int, y: int) -> Optional[str]:
        """
        Get the intersection value at the given coordinates of the board

        Args:
            x (int): Column on the board
            y (int): Row on the board

        Returns:
            Optional[str]: Value (if exists).
        """
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.board[y][x]

    def territory(self, x: int, y: int) -> Tuple[str, Set[Point]]:
        """Find the owner and the territories given a coordinate on
           the board

        Args:
            x (int): Column on the board
            y (int): Row on the board

        Returns:
            (str, set): A tuple, the first element being the owner
                        of that area.  One of "W", "B", " ".  The
                        second being a set of coordinates, representing
                        the owner's territories.
        """
        def visit(p: int, q: int) -> None:
            owner = self.get_value(p, q)
            if owner in {WHITE, BLACK}:
                owners.add(owner)
            elif owner and (p, q) not in points:
                points.add((p, q))
                for dx, dy in COMPASS:
                    visit(p + dx, q + dy)

        owners, points = set(), set()
        visit(x, y)

        if len(points) == 0:
            if len(owners) == 0:
                raise ValueError('Out of bounds!')
            return NONE, points
        return owners.pop() if len(owners) == 1 else NONE, points

    def territories(self) -> Dict[str, Set[Point]]:
        """Find the owners and the territories of the whole board

        Args:
            none

        Returns:
            dict(str, set): A dictionary whose key being the owner
                        , i.e. "W", "B", " ".  The value being a set
                        of coordinates owned by the owner.
        """
        owned_territories = {owner: set() for owner in [NONE, WHITE, BLACK]}

        for x in range(self.width):
            for y in range(self.height):
                if any((x, y) in visited for visited in owned_territories.values()):
                    continue
                owner, territory = self.territory(x, y)
                owned_territories[owner].update(territory)

        return owned_territories
