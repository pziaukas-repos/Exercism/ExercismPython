from typing import List, Optional, Tuple

Dominoes = List[Tuple[int, int]]


def can_chain(dominoes: Dominoes) -> Optional[Dominoes]:
    return chain(dominoes[:1], dominoes[1:]) if len(dominoes) else []


def chain(solved: Dominoes, remaining: Dominoes) -> Optional[Dominoes]:
    tail = solved[-1][1]
    if not remaining:
        return solved if solved[0][0] == tail else None

    for k, domino in enumerate(remaining):
        if tail in domino:
            if domino[0] != tail:
                domino = domino[::-1]
            solution = chain(solved + [domino], remaining[:k] + remaining[k + 1:])
            if solution:
                return solution
    return None
