from typing import List

MASK_SEVEN = 0b01111111
MASK_FIRST = 0b10000000


def encode(numbers: List[int]) -> List[int]:
    pairs = []
    for number in numbers:
        buffer = [number & MASK_SEVEN]
        number >>= 7
        while number:
            buffer.append(MASK_FIRST | number & MASK_SEVEN)
            number >>= 7
        pairs.extend(reversed(buffer))
    return pairs


def decode(pairs: List[int]) -> List[int]:
    if pairs[-1] & MASK_FIRST:
        raise ValueError('Malformed pairs!')
    numbers = []
    buffer = 0
    for pair in pairs:
        buffer <<= 7
        buffer += pair & MASK_SEVEN
        if not pair & MASK_FIRST:
            numbers.append(buffer)
            buffer = 0
    return numbers
