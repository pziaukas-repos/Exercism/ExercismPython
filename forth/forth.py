from typing import List, Tuple


class StackUnderflowError(Exception):
    pass


class Stack(list):
    def pop(self, size: int = 1) -> Tuple[int, ...]:
        if len(self) < size:
            raise StackUnderflowError('Not enough items in the stack!')
        tail = self[-size:]
        del self[-size:]
        return tuple(tail)

    def push(self, *items: int) -> None:
        for item in items:
            self.append(item)


def evaluate(input_data: List[str]) -> List[int]:
    stack = Stack()
    macros = dict()

    for command in input_data:
        words = command.upper().split()
        while len(words):
            word = words.pop(0)
            if word.isdecimal():
                stack.push(int(word))
            elif word in macros:
                words = macros[word] + words
            elif word == ':':
                title = words.pop(0)
                if title.isdecimal():
                    raise ValueError('Cannot redefine numbers!')
                if words.pop() != ';':
                    raise ValueError("The definition must end with ';'!")
                macros[title] = [new_word for word in words for new_word in macros.get(word, [word])]
                del words[:]
            elif word == '+':
                a, b = stack.pop(2)
                stack.push(a + b)
            elif word == '-':
                a, b = stack.pop(2)
                stack.push(a - b)
            elif word == '*':
                a, b = stack.pop(2)
                stack.push(a * b)
            elif word == '/':
                a, b = stack.pop(2)
                stack.push(a // b)
            elif word == 'DUP':
                a, = stack.pop()
                stack.push(a, a)
            elif word == 'DROP':
                stack.pop()
            elif word == 'SWAP':
                a, b = stack.pop(2)
                stack.push(b, a)
            elif word == 'OVER':
                a, b = stack.pop(2)
                stack.push(a, b, a)
            else:
                raise ValueError('Unknown command!')

    return stack
