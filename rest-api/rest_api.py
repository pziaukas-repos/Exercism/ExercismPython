from functools import partialmethod
import json
from typing import Optional


class RestAPI:
    def __init__(self, database: dict = None) -> None:
        self._db = {user['name']: user for user in database['users']}
        self._routes = {
            'get': {
                '/users': self.get_users,
            },
            'post': {
                '/add': self.post_add,
                '/iou': self.post_iou,
            }
        }

    def handle_request(self, url: str, payload: Optional[str] = None, method: str = 'get'):
        if method not in self._routes:
            response = RequestError(405, method)
        elif url not in self._routes[method]:
            response = RequestError(404, url)
        else:
            handler = self._routes[method][url]
            try:
                response = handler(json.loads(payload) if payload else None)
            except KeyError as ex:
                response = RequestError(400, f'{ex} not found')
        return json.dumps(response)

    get = partialmethod(handle_request, method='get')
    post = partialmethod(handle_request, method='post')

    def get_users(self, payload: Optional[dict]) -> dict:
        users = [self._db[name] for name in payload['users']] if payload else list(self._db.values())
        return {'users': sorted(users, key=lambda x: x['name']) if users else []}

    def post_add(self, payload: dict) -> dict:
        name = payload['user']

        new_user = {
            'name': name,
            'owes': {},
            'owed_by': {},
            'balance': 0
        }
        self._db[name] = self._db.get(name, new_user)
        return self._db[name]

    def post_iou(self, payload: dict) -> dict:
        lender_name = payload['lender']
        borrower_name = payload['borrower']
        amount = payload['amount']

        lender = self._db[lender_name]
        lender['owed_by'][borrower_name] = lender['owed_by'].get(borrower_name, 0) + amount
        lender['balance'] += amount

        borrower = self._db[borrower_name]
        borrower['owes'][lender_name] = borrower['owes'].get(lender_name, 0) + amount
        borrower['balance'] -= amount

        # reconcile
        for user, partner_name in [(lender, borrower_name), (borrower, lender_name)]:
            buffer = user['owed_by'].pop(partner_name, 0) - user['owes'].pop(partner_name, 0)
            if buffer > 0:
                user['owed_by'][partner_name] = buffer
            elif buffer < 0:
                user['owes'][partner_name] = -buffer

        return self.get_users({'users': [lender_name, borrower_name]})


class RequestError(dict):
    default_message = {
        400: 'Malformed request',
        404: 'Address not found',
        405: 'Method not allowed'
    }

    def __init__(self, code: int, message: Optional[str] = None) -> None:
        full_message = RequestError.default_message[code] + (f': {message}' if message else '.')
        super().__init__(code=code, message=full_message)
