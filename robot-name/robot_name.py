from secrets import choice
import string


class Robot(object):
    def __init__(self):
        self.name = ''
        self.reset()

    def reset(self) -> None:
        name_first = ''.join(choice(string.ascii_uppercase) for k in range(2))
        name_second = ''.join(choice(string.digits) for k in range(3))
        self.name = f'{name_first}{name_second}'
