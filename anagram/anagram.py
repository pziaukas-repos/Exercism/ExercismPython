from typing import List


def is_matching(original: str, candidate: str) -> bool:
    return len(set(str(sorted(list(x))) for x in [original, candidate])) == 1


def find_anagrams(word: str, candidates: List[str]) -> List[str]:
    word_lower = word.lower()
    return [x for x in candidates if is_matching(word_lower, x.lower()) and word_lower != x.lower()]
