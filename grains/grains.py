def square(integer_number: int) -> int:
    if not 0 < integer_number <= 64:
        raise ValueError('Nonexistent square!')
    return 2 ** (integer_number - 1)


def total(integer_number: int) -> int:
    if not 0 < integer_number <= 64:
        raise ValueError('Nonexistent square!')
    return 2 ** integer_number - 1
