def say_bottle(number: int) -> str:
    return {1: '1 bottle',
            0: 'no more bottles'}\
        .get(number, f'{number} bottles')


def say_action(number: int) -> str:
    return {1: 'Take it down and pass it around',
            0: 'Go to the store and buy some more'}\
        .get(number, 'Take one down and pass it around')


def recite(start: int, take: int = 1) -> list:
    lyrics = [f"{say_bottle(start).capitalize()} of beer on the wall, {say_bottle(start)} of beer.",
              f"{say_action(start)}, {say_bottle((start - 1) % 100)} of beer on the wall."]

    if take == 1:
        return lyrics
    return lyrics + [''] + recite(start - 1, take - 1)
