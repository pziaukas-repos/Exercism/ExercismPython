from typing import List


WINK = 'wink'
BLINK = 'double blink'
CLOSE = 'close your eyes'
JUMP = 'jump'

CODE_BOOK = {x: 2 ** k for k, x in enumerate([WINK, BLINK, CLOSE, JUMP])}


def commands(code: int) -> List[str]:
    shake = [x for x, n in CODE_BOOK.items() if code & n]
    return shake if not code & 2**4 else shake[::-1]


def secret_code(actions: List[str]) -> int:
    shake = [CODE_BOOK[x] for x in actions]
    return sum(shake) if shake == sorted(shake) else sum(shake) + 2**4
