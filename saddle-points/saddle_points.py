from typing import Dict, List

Matrix = List[List[int]]
SaddlePoint = Dict[str, int]


def saddle_points(matrix: Matrix) -> List[SaddlePoint]:
    if len(set(map(len, matrix))) > 1:
        raise ValueError('This is not a matrix!')

    matrix_t = list(zip(*matrix))
    points = []
    for i, row in enumerate(matrix):
        for j, x in enumerate(row):
            if x == max(row) and x == min(matrix_t[j]):
                points.append({'row': i + 1, 'column': j + 1})
    return points or [{}]
