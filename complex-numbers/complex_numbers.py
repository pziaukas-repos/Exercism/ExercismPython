import math


class ComplexNumber:
    def __init__(self, real: float, imaginary: float) -> None:
        self.real = real
        self.imaginary = imaginary

    def __eq__(self, other: 'ComplexNumber') -> bool:
        return self.real == other.real and self.imaginary == other.imaginary

    def __add__(self, other: 'ComplexNumber') -> 'ComplexNumber':
        real = self.real + other.real
        imaginary = self.imaginary + other.imaginary
        return ComplexNumber(real, imaginary)

    def __mul__(self, other: 'ComplexNumber') -> 'ComplexNumber':
        real = self.real * other.real - self.imaginary * other.imaginary
        imaginary = self.imaginary * other.real + self.real * other.imaginary
        return ComplexNumber(real, imaginary)

    def __sub__(self, other: 'ComplexNumber') -> 'ComplexNumber':
        real = self.real - other.real
        imaginary = self.imaginary - other.imaginary
        return ComplexNumber(real, imaginary)

    def __truediv__(self, other: 'ComplexNumber') -> 'ComplexNumber':
        norm = abs(other) ** 2
        real = (self.real * other.real + self.imaginary * other.imaginary) / norm
        imaginary = (self.imaginary * other.real - self.real * other.imaginary) / norm
        return ComplexNumber(real, imaginary)

    def __abs__(self) -> float:
        return math.sqrt(self.real ** 2 + self.imaginary ** 2)

    def conjugate(self) -> 'ComplexNumber':
        return ComplexNumber(self.real, -self.imaginary)

    def exp(self) -> 'ComplexNumber':
        radius = math.exp(self.real)
        real = radius * math.cos(self.imaginary)
        imaginary = radius * math.sin(self.imaginary)
        return ComplexNumber(real, imaginary)
