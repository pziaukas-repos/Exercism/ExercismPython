"""Card Games solution."""


def get_rounds(number: int) -> list[int]:
    """Calculate the current round and the next two that are coming up.

     :param number: int - current round number.
     :return: list - current round and the two that follow.
    """

    return [number + k for k in range(3)]


def concatenate_rounds(rounds_1: list[int], rounds_2: list[int]) -> list[int]:
    """Calculate a single list of all the rounds.

    :param rounds_1: list - first rounds played.
    :param rounds_2: list - second set of rounds played.
    :return: list - all rounds played.
    """

    return rounds_1 + rounds_2


def list_contains_round(rounds: list[int], number: int) -> bool:
    """Find if the round is in the list of rounds played.

    :param rounds: list - rounds played.
    :param number: int - round number.
    :return:  bool - was the round played?
    """

    return number in rounds


def card_average(hand: list[int]) -> float:
    """Calculate the average value of a hand of Black Joe.

    :param hand: list - cards in hand.
    :return:  float - average value of the cards in the hand.
    """

    return sum(hand) / len(hand)


def approx_average_is_average(hand: list[int]) -> bool:
    """Check if the average coincides with the approximation techniques.

    :param hand: list - cards in hand.
    :return: bool - is approximate average the same as true average?
    """

    real_average = card_average(hand)
    is_same = False
    # Median value
    if hand[len(hand) // 2] == real_average:
        is_same = True
    # Average of the first and the last cards
    if card_average([hand[0], hand[-1]]) == real_average:
        is_same = True
    return is_same


def average_even_is_average_odd(hand: list[int]) -> bool:
    """Calculate if the average of the cards at even indexes is the same as the average of the cards at odd indexes.

    :param hand: list - cards in hand.
    :return: bool - are even and odd averages equal?
    """

    return card_average(hand[::2]) == card_average(hand[1::2])


def maybe_double_last(hand: list[int]) -> list[int]:
    """Double the last card if it's a Jack.

    :param hand: list - cards in hand.
    :return: list - hand with Jacks (if present) value doubled.
    """

    if hand[-1] == 11:
        hand[-1] *= 2
    return hand
