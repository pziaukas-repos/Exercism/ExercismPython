from typing import List


class CustomSet:
    def __init__(self, elements: List[int] = []) -> None:
        self.elements = {x: None for x in elements}

    def __bool__(self) -> bool:
        return len(self.elements) > 0

    def __contains__(self, element: int) -> bool:
        return element in self.elements

    def __eq__(self, other: 'CustomSet') -> bool:
        return self.elements == other.elements

    def __add__(self, other: 'CustomSet') -> 'CustomSet':
        return CustomSet([x for y in (self, other) for x in y.elements.keys()])

    def __sub__(self, other: 'CustomSet') -> 'CustomSet':
        return CustomSet([x for x in self.elements if x not in other])

    def add(self, element: int) -> None:
        self.elements[element] = None

    def isempty(self) -> bool:
        return not self

    def issubset(self, other: 'CustomSet') -> bool:
        return (self - other).isempty()

    def isdisjoint(self, other: 'CustomSet') -> bool:
        return self - other == self

    def intersection(self, other: 'CustomSet') -> 'CustomSet':
        return (self + other) - (self - other) - (other - self)
