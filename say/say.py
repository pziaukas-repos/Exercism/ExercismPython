def say(number: int) -> str:
    if not 0 <= number < 10 ** 12:
        raise ValueError('Number is out of bounds!')
    if number < 10:
        return ['zero', 'one', 'two', 'three', 'four',
                'five', 'six', 'seven', 'eight', 'nine'][number]
    elif number < 20:
        return ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
                'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'][number - 10]
    elif number < 100:
        separator = '-'
        quotient, remainder = divmod(number, 10)
        major = ['twenty', 'thirty', 'forty', 'fifty',
                 'sixty', 'seventy', 'eighty', 'ninety'][quotient - 2]
    else:
        if number < 1000:
            separator = ' and '
            divisor, divisor_name = 100, 'hundred'
        else:
            separator = ' '
            divisor, divisor_name = next((x, y) for x, y in
                                         [(1000, 'thousand'), (10 ** 6, 'million'), (10 ** 9, 'billion')]
                                         if number < 1000 * x)
        quotient, remainder = divmod(number, divisor)
        major = f'{say(quotient)} {divisor_name}'
    return f'{major}{separator}{say(remainder)}' if remainder else major
