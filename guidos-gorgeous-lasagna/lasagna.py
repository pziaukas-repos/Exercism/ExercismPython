EXPECTED_BAKE_TIME = 40
PREPARATION_TIME = 2


def bake_time_remaining(elapsed_bake_time: int) -> int:
    """Calculate the remaining bake time.

    Function that takes the actual minutes the lasagna has been in the oven as
    an argument and returns how many minutes the lasagna still needs to bake
    based on the `EXPECTED_BAKE_TIME`.
    """
    return EXPECTED_BAKE_TIME - elapsed_bake_time


def preparation_time_in_minutes(number_of_layers: int) -> int:
    """Calculate the total preparation time.

    Function that takes the number of layers in the lasagna as an argument
    and returns the total preparation time based on the preparation time
    of a single layer as defined by `PREPARATION_TIME`.
    """
    return PREPARATION_TIME * number_of_layers


def elapsed_time_in_minutes(number_of_layers: int, elapsed_bake_time: int) -> int:
    """Calculate the overall elapsed time.

    Function that takes the number of layers in the lasagna and the elapsed bake time
    as arguments and returns the overall elapsed time of the process.
    """
    return preparation_time_in_minutes(number_of_layers) + elapsed_bake_time
