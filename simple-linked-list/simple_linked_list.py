from typing import Iterator, List, Optional


class Node:
    def __init__(self, value: int, next_node: 'Node' = None) -> None:
        self._value = value
        self._next = next_node

    def value(self) -> int:
        return self._value

    def next(self) -> Optional['Node']:
        return self._next


class LinkedList:
    def __init__(self, values: List[int] = None) -> None:
        self._head = None
        for value in values or []:
            self.push(value)

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def __iter__(self) -> Iterator[int]:
        node = self._head
        while node:
            yield node.value()
            node = node.next()

    def head(self) -> Node:
        if self._head is None:
            raise EmptyListException('Empty list has no head!')
        return self._head

    def push(self, value: int) -> None:
        self._head = Node(value, self._head)

    def pop(self) -> int:
        if self._head is None:
            raise EmptyListException('No node to pop!')
        head = self.head()
        self._head = head.next()
        return head.value()

    def reversed(self) -> 'LinkedList':
        result = LinkedList()
        for value in self:
            result.push(value)
        return result


class EmptyListException(Exception):
    pass
