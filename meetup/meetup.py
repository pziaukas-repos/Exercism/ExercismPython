from datetime import date, timedelta


class MeetupDayException(Exception):
    pass


def meetup(year: int, month: int, which: str, day_of_the_week: str) -> date:
    week_map = {'Monday': 0, 'Tuesday': 1, 'Wednesday': 2, 'Thursday': 3, 'Friday': 4, 'Saturday': 5, 'Sunday': 6}

    if which == 'last':
        last_date = date(year, month, 31) if month == 12 else date(year, month + 1, 1) - timedelta(days=1)
        day = last_date.day - (last_date.weekday() - week_map[day_of_the_week]) % 7
    elif which == 'teenth':
        day = 13 + (week_map[day_of_the_week] - date(year, month, 13).weekday()) % 7
    else:
        # otherwise which = 'nth'
        day = 1 + (int(which[0]) - 1) * 7 + (week_map[day_of_the_week] - date(year, month, 1).weekday()) % 7

    try:
        return date(year, month, day)
    except ValueError:
        raise MeetupDayException('The described day is nonexistent!')
