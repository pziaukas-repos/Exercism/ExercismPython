def is_pangram(sentence):
    return len(set(x.upper() for x in sentence if x.isalpha())) == 26
