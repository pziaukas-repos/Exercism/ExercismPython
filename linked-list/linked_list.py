from typing import Iterator


class Node:
    def __init__(self, value: int,
                 succeeding: 'Node' = None, previous: 'Node' = None) -> None:
        self.value = value
        self.succeeding = succeeding
        self.previous = previous


class LinkedList:
    def __init__(self) -> None:
        self._head = self._tail = None

    def __iter__(self) -> Iterator[int]:
        node = self._head
        while node:
            yield node.value
            node = node.succeeding

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def push(self, value: int) -> None:
        node = Node(value, previous=self._tail)
        try:
            self._tail.succeeding = node
        except AttributeError:
            self._head = node
        finally:
            self._tail = node

    def pop(self) -> int:
        tail = self._tail
        try:
            tail.previous.succeeding = None
        except AttributeError:
            self._head = None
        finally:
            self._tail = tail.previous
        return tail.value

    def unshift(self, value: int) -> None:
        node = Node(value, succeeding=self._head)
        try:
            self._head.previous = node
        except AttributeError:
            self._tail = node
        finally:
            self._head = node

    def shift(self) -> int:
        head = self._head
        try:
            head.succeeding.previous = None
        except AttributeError:
            self._tail = None
        finally:
            self._head = head.succeeding
        return head.value
