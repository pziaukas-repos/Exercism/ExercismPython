from dataclasses import dataclass
from typing import Iterator, List


@dataclass
class TreeNode:
    data: str
    left: 'TreeNode' = None
    right: 'TreeNode' = None

    def walk(self) -> Iterator[str]:
        if self.left:
            for value in self.left.walk():
                yield value
        yield self.data
        if self.right:
            for value in self.right.walk():
                yield value


class BinarySearchTree:
    def __init__(self, values: List[str]) -> None:
        self.root = None
        for value in values:
            self.insert(value)

    def data(self) -> 'TreeNode':
        return self.root

    def insert(self, value: str) -> None:
        parent = None
        node = self.root
        child = TreeNode(value)

        while node:
            parent = node
            node = node.right if value > node.data else node.left

        if parent:
            if parent.data < value:
                parent.right = child
            else:
                parent.left = child
        else:
            self.root = child

    def sorted_data(self) -> List[str]:
        return [value for value in self.root.walk()]
