from dataclasses import dataclass
from typing import List


@dataclass
class Animal:
    name: str
    consequence: str
    description: str = ''


spider_action = 'wriggled and jiggled and tickled inside her'
ANIMALS = [
    Animal('fly', "I don't know why she swallowed the fly. Perhaps she'll die."),
    Animal('spider', f'It {spider_action}.', f' that {spider_action}'),
    Animal('bird', 'How absurd to swallow a bird!'),
    Animal('cat', 'Imagine that, to swallow a cat!'),
    Animal('dog', 'What a hog, to swallow a dog!'),
    Animal('goat', 'Just opened her throat and swallowed a goat!'),
    Animal('cow', "I don't know how she swallowed a cow!"),
    Animal('horse', "She's dead, of course!")
]


def say_lyrics(verse: int) -> List[str]:
    k, *other_k = reversed(range(verse)) if verse != len(ANIMALS) else [len(ANIMALS) - 1]
    verse = [f'I know an old lady who swallowed a {ANIMALS[k].name}.', ANIMALS[k].consequence]
    if other_k:
        for k in other_k:
            verse.append(f'She swallowed the {ANIMALS[k + 1].name} '
                         f'to catch the {ANIMALS[k].name}{ANIMALS[k].description}.')
        verse.append(ANIMALS[k].consequence)
    return verse


def recite(start_verse: int, end_verse: int) -> List[str]:
    verses = []
    for k in range(start_verse, end_verse + 1):
        if verses:
            verses.append('')
        verses.extend(say_lyrics(k))
    return verses
