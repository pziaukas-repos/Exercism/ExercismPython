from dataclasses import dataclass
from typing import List, Union


class Zipper:
    def __init__(self, focus: 'BinaryTree' = None, ancestors: List[dict] = None) -> None:
        self.focus = focus
        self.ancestors = ancestors or []

    @property
    def value(self) -> int:
        try:
            return self.focus.value
        except AttributeError:
            raise ValueError('No value!')

    @property
    def left(self) -> 'Zipper':
        context = self.focus.__dict__
        focus = context.pop('left')
        ancestors = self.ancestors + [context]
        return Zipper(focus, ancestors)

    @property
    def right(self) -> 'Zipper':
        context = self.focus.__dict__
        focus = context.pop('right')
        ancestors = self.ancestors + [context]
        return Zipper(focus, ancestors)

    @property
    def up(self) -> 'Zipper':
        context = {key: self.focus for key in ['left', 'right']}
        context.update(self.ancestors[-1])
        focus = BinaryTree(**context)
        ancestors = self.ancestors[:-1]
        return Zipper(focus, ancestors)

    @property
    def root(self) -> 'Zipper':
        zipper = Zipper(self.focus, self.ancestors)
        while zipper.ancestors:
            zipper = zipper.up
        return zipper

    def insert(self, value: Union[int, 'BinaryTree', 'Zipper']) -> 'Zipper':
        if isinstance(value, Zipper):
            focus = value.root.focus
        elif isinstance(value, BinaryTree):
            focus = value
        else:
            focus = BinaryTree(value)
        return Zipper(focus, self.ancestors)


@dataclass(frozen=True)
class BinaryTree:
    value: int
    left: 'BinaryTree' = None
    right: 'BinaryTree' = None
