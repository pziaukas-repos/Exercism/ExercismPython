from random import randint


class Character:
    def __init__(self) -> None:
        abilities = {'strength', 'dexterity', 'constitution',
                     'intelligence', 'wisdom', 'charisma'}

        for ability in abilities:
            setattr(self, ability, self.ability())
        self.hitpoints = 10 + modifier(self.constitution)

    @staticmethod
    def ability():
        dice = [randint(1, 6) for _ in range(4)]
        return sum(dice) - min(dice)


def modifier(score: int) -> int:
    return (score - 10) // 2
