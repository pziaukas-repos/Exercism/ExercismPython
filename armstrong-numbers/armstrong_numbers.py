def is_armstrong_number(number: int) -> bool:
    digits = [int(k) for k in str(number)]
    return number == sum(k ** len(digits) for k in digits)
