from collections import defaultdict
from dataclasses import dataclass
from typing import List, Optional, Tuple


@dataclass(frozen=True)
class Point:
    x: int = 0
    y: int = 0

    def __add__(self, other: 'Point') -> 'Point':
        return Point(self.x + other.x, self.y + other.y)

    def __mul__(self, multiplier: int) -> 'Point':
        return Point(multiplier * self.x, multiplier * self.y)

    def __rmul__(self, multiplier: int) -> 'Point':
        return self.__mul__(multiplier)


SHIFTS = [Point(x, y) for x in [-1, 0, 1] for y in [-1, 0, 1] if x or y]


class WordSearch:
    def __init__(self, puzzle: List[str]) -> None:
        self.locations = defaultdict(set)
        for y, puzzle_line in enumerate(puzzle):
            for x, letter in enumerate(puzzle_line):
                self.locations[letter].add(Point(x, y))

    def search(self, word: str) -> Optional[Tuple[Point, Point]]:
        for shift in SHIFTS:
            for start in self.locations[word[0]]:
                if all(start + n * shift in self.locations[letter] for n, letter in enumerate(word[1:], 1)):
                    return start, start + (len(word) - 1) * shift
