from itertools import permutations
from typing import Dict, Iterator, Optional

Solution = Dict[str, int]


def solve(puzzle: str) -> Optional[Solution]:
    def fit(known_solution: Solution, depth: int = 0, carry: int = 0) -> Iterator[Solution]:
        if depth == len(result) and carry == 0:
            yield known_solution

        letters = tuple({x for x in levels[depth] if x not in known_solution.keys()})
        digits_remaining = [d for d in range(10) if d not in known_solution.values()]
        for digits in permutations(digits_remaining, len(letters)):
            if 0 in digits and letters[digits.index(0)] in firsts:
                continue

            solution = {**known_solution, **dict(zip(letters, digits))}
            new_carry, remainder = divmod(carry + sum(solution[x] for x in levels[depth]), 10)

            if result[depth] in solution:
                if solution[result[depth]] != remainder:
                    continue
            else:
                if remainder in solution.values():
                    continue
                elif remainder == 0 and result[depth] in firsts:
                    continue
                solution[result[depth]] = remainder

            yield from fit(solution, depth + 1, new_carry)

    words = [word for word in puzzle.split() if word.isalpha()]
    if max(len(word) for word in words[:-1]) > len(words[-1]):
        return None
    firsts = {word[0] for word in words}

    result = list(reversed(words[-1]))
    levels = [[] for _ in result]
    for word in words[:-1]:
        for n, letter in enumerate(reversed(word)):
            levels[n].append(letter)

    return next(fit(known_solution={}), None)
