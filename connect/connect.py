from dataclasses import dataclass
from itertools import permutations
from typing import Callable, Set


@dataclass(frozen=True)
class Tile:
    x: int = 0
    y: int = 0
    z: int = 0

    def __add__(self, other: 'Tile') -> 'Tile':
        return Tile(self.x + other.x, self.y + other.y, self.z + other.z)

    def __mul__(self, multiplier: int) -> 'Tile':
        return Tile(multiplier * self.x, multiplier * self.y, multiplier * self.z)

    def __rmul__(self, multiplier: int) -> 'Tile':
        return self.__mul__(multiplier)


SHIFTS = [Tile(*coordinates) for coordinates in permutations([-1, 0, 1])]


class ConnectGame:
    """
    Tiles are represented using hex grid cube coordinates.
    """
    def __init__(self, board: str) -> None:
        self.board = {}
        self.dx = Tile(x=1, z=-1)
        self.dy = Tile(y=1, z=-1)
        origin = tile = Tile()
        for y, values in enumerate(board.splitlines()):
            for x, value in enumerate(values.strip().split()):
                tile = origin + x * self.dx + y * self.dy
                self.board[tile] = value
        self.boundary = tile

    def get_winner(self) -> str:
        rules = [
            ('X', self.dy, lambda t: t.x == self.boundary.x),
            ('O', self.dx, lambda t: t.y == self.boundary.y)
        ]
        for rule in rules:
            value, shift, condition = rule
            tile = Tile()
            while tile in self.board:
                if self.board[tile] == value and self.trace(tile, condition, set()):
                    return value
                tile += shift
        return ''

    def trace(self, tile: Tile, condition: Callable[[Tile], bool], track: Set[Tile]) -> bool:
        if condition(tile):
            return True

        track.add(tile)
        return any(self.trace(tile + shift, condition, track) for shift in SHIFTS
                   if tile + shift not in track and self.board[tile] == self.board.get(tile + shift))
