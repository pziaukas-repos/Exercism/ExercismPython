from typing import List


def say_lyrics(verse: int) -> str:
    day_list = ['first', 'second', 'third', 'fourth',
                'fifth', 'sixth', 'seventh', 'eighth',
                'ninth', 'tenth', 'eleventh', 'twelfth']
    say_day = f'On the {day_list[verse - 1]} day of Christmas my true love gave to me'

    item_list = ['a Partridge in a Pear Tree', 'two Turtle Doves',
                 'three French Hens', 'four Calling Birds',
                 'five Gold Rings', 'six Geese-a-Laying',
                 'seven Swans-a-Swimming', 'eight Maids-a-Milking',
                 'nine Ladies Dancing', 'ten Lords-a-Leaping',
                 'eleven Pipers Piping', 'twelve Drummers Drumming']
    say_items = (', '.join(item_list[verse - 1:0:-1] + ['and ']) if verse > 1 else '') + item_list[0]
    return f'{say_day}: {say_items}.'


def recite(start_verse: int, end_verse: int) -> List[str]:
    return [say_lyrics(k) for k in range(start_verse, end_verse + 1)]
