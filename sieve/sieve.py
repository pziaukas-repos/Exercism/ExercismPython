from typing import Iterable


def lazy_sieve(limit: int) -> Iterable[int]:
    denied = set()
    for k in range(2, limit + 1):
        if k not in denied:
            denied.update(c for c in range(k, limit + 1, k))
            yield k


def primes(limit: int) -> list:
    return [x for x in lazy_sieve(limit)]
