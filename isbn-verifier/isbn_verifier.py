import re


def is_valid(isbn):
    isbn_clean = isbn.replace('-', '')
    if not (re.match('^[0-9]{9}[0-9X]$', isbn_clean)):
        return False

    checksum = 0
    for k in range(9):
        checksum += (10 - k) * int(isbn_clean[k])
    checksum += 10 if isbn_clean[9] == 'X' else int(isbn_clean[9])

    return checksum % 11 == 0
