from itertools import zip_longest


def transpose(input_lines: str) -> str:
    transposed = zip_longest(*input_lines.splitlines(), fillvalue='_')
    return '\n'.join(''.join(row).rstrip('_').replace('_', ' ') for row in transposed)
