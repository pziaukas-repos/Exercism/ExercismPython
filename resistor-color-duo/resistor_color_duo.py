from typing import List

CODE_BY_COLOR = {c: n for n, c in enumerate(['black', 'brown', 'red', 'orange', 'yellow',
                                             'green', 'blue', 'violet', 'grey', 'white'])}


def value(colors: List[str]) -> int:
    return 10 * CODE_BY_COLOR[colors[0]] + CODE_BY_COLOR[colors[1]]
