from typing import Callable, List, Union

Sides = List[Union[int, float]]


def valid_triangle(func: Callable) -> Callable:
    def wrapper(sides: Sides) -> bool:
        return sum(sides) > 2 * max(sides) and func(sides)
    return wrapper


@valid_triangle
def equilateral(sides: Sides) -> bool:
    return len(set(sides)) == 1


@valid_triangle
def isosceles(sides: Sides) -> bool:
    return len(set(sides)) < 3


@valid_triangle
def scalene(sides: Sides) -> bool:
    return len(set(sides)) == 3
