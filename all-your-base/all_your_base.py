from typing import List

Digits = List[int]


def rebase(input_base: int, digits: Digits, output_base: int) -> Digits:
    if input_base < 2 or output_base < 2:
        raise ValueError('Invalid base!')
    if not all(0 <= digit < input_base for digit in digits):
        raise ValueError('Digit is out of bounds!')

    value = 0
    for digit in digits:
        value = value * input_base + digit

    output_digits = []
    while value:
        value, digit = divmod(value, output_base)
        output_digits.insert(0, digit)

    return output_digits
