def hello(name: str = 'World') -> str:
    return f'Hello, {name}!'
