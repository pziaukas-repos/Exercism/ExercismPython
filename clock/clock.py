class Clock:
    def __init__(self, hour: int, minute: int) -> None:
        hour_extra, self.minute = divmod(minute, 60)
        self.hour = (hour + hour_extra) % 24

    def __repr__(self) -> str:
        return f'{self.hour:02}:{self.minute:02}'

    def __eq__(self, other: 'Clock') -> bool:
        return self.__dict__ == other.__dict__

    def __add__(self, minutes: int) -> 'Clock':
        return Clock(self.hour, self.minute + minutes)

    def __sub__(self, minutes: int) -> 'Clock':
        return Clock(self.hour, self.minute - minutes)
