def decode(string):
    result = ''

    k = 0
    while k < len(string):
        run = ''
        while string[k].isdigit():
            run += string[k]
            k += 1
        if run:
            result += string[k] * int(run)
        else:
            result += string[k]
        k += 1

    return result


def encode(string):
    result = ''

    k = 0
    while k < len(string):
        x = string[k]
        run = 0
        try:
            while string[k] is x:
                k += 1
                run += 1
        except IndexError:
            pass
        if run > 1:
            result += str(run) + x
        else:
            result += x

    return result
