from itertools import cycle
from typing import List

Matrix = List[List[int]]


def spiral_matrix(size: int) -> Matrix:
    directions = cycle([(0, 1), (1, 0), (0, -1), (-1, 0)])
    matrix = [[None for _ in range(size)] for _ in range(size)]

    x, y = 0, 0
    dx, dy = next(directions)
    for number in range(1, size ** 2 + 1):
        matrix[x][y] = number
        if x + dx in (-1, size) or y + dy in (-1, size) or matrix[x + dx][y + dy] is not None:
            dx, dy = next(directions)
        x += dx
        y += dy

    return matrix
