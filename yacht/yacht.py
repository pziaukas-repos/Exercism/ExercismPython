from typing import Callable, List

Dice = List[int]


# Score categories
def YACHT(dice: Dice) -> int:
    return 50 if len(set(dice)) == 1 else 0


def FULL_HOUSE(dice: Dice) -> int:
    return sum(dice) if len(set(dice)) == 2 and dice.count(dice[0]) in [2, 3] else 0


def FOUR_OF_A_KIND(dice: Dice) -> int:
    return 4 * sorted(dice)[2] if len(set(dice)) < 3 and dice.count(dice[0]) in [1, 4, 5] else 0


def LITTLE_STRAIGHT(dice: Dice) -> int:
    return 30 if sorted(dice) == [1, 2, 3, 4, 5] else 0


def BIG_STRAIGHT(dice: Dice) -> int:
    return 30 if sorted(dice) == [2, 3, 4, 5, 6] else 0


def CHOICE(dice: Dice) -> int:
    return sum(dice)


def score_value(value: int) -> Callable[[Dice], int]:
    return lambda dice: sum(x for x in dice if x == value)


ONES = score_value(1)
TWOS = score_value(2)
THREES = score_value(3)
FOURS = score_value(4)
FIVES = score_value(5)
SIXES = score_value(6)


def score(dice: List[int], category: Callable[[Dice], int]) -> int:
    return category(dice)
