def gcd(a: int, b: int) -> int:
    while b:
        a, b = b, a % b
    return a


class Rational(object):
    def __init__(self, numer: int, denom: int):
        divisor = gcd(numer, denom)
        self.numer = abs(numer // divisor) if numer * denom >= 0 else -abs(numer // divisor)
        self.denom = abs(denom // divisor)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return f'{self.numer}/{self.denom}'

    def __add__(self, other):
        return Rational(self.numer * other.denom + other.numer * self.denom, self.denom * other.denom)

    def __sub__(self, other):
        return Rational(self.numer * other.denom - other.numer * self.denom, self.denom * other.denom)

    def __mul__(self, other):
        return Rational(self.numer * other.numer, self.denom * other.denom)

    def __truediv__(self, other):
        return Rational(self.numer * other.denom, self.denom * other.numer)

    def __abs__(self):
        return Rational(abs(self.numer), self.denom)

    def __pow__(self, power: int):
        if power > 0:
            return Rational(self.numer ** power, self.denom ** power)
        if power < 0:
            return Rational(self.denom ** power, self.numer ** power)
        else:
            return Rational(1, 1)

    def __rpow__(self, base: int):
        return base ** (self.numer / self.denom)
