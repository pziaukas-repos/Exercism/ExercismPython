from typing import Dict, List

Item = Dict[str, int]


def maximum_value(maximum_weight: int, items: List[Item], position: int = 0) -> int:
    if position == len(items):
        return 0

    alternative_value = maximum_value(maximum_weight, items, position + 1)
    remaining_weight = maximum_weight - items[position]['weight']
    if remaining_weight >= 0:
        value = items[position]['value'] + maximum_value(remaining_weight, items, position + 1)
        return max(value, alternative_value)
    return alternative_value
