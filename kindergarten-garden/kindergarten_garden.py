from typing import List

DEFAULT_STUDENTS = ['Alice', 'Bob', 'Charlie', 'David', 'Eve', 'Fred',
                    'Ginny', 'Harriet', 'Ileana', 'Joseph', 'Kincaid', 'Larry']


class Garden:
    plant_names = {plant[0]: plant for plant in ['Clover', 'Grass', 'Radishes', 'Violets']}

    def __init__(self, diagram: str, students: List[str] = None) -> None:
        self.students = sorted(students or DEFAULT_STUDENTS)
        self.rows = diagram.splitlines()
        self.student_slices = {student: slice(2 * k, 2 * k + 2) for k, student in enumerate(self.students)}

    def plants(self, student: str) -> List[str]:
        return [Garden.plant_names[plant] for row in self.rows for plant in row[self.student_slices[student]]]
