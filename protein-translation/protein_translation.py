from itertools import takewhile
from textwrap import wrap
from typing import List


CATALOG = [(['AUG'], 'Methionine'),
           (['UUU', 'UUC'], 'Phenylalanine'),
           (['UUA', 'UUG'], 'Leucine'),
           (['UCU', 'UCC', 'UCA', 'UCG'], 'Serine'),
           (['UAU', 'UAC'], 'Tyrosine'),
           (['UGU', 'UGC'], 'Cysteine'),
           (['UGG'], 'Tryptophan'),
           (['UAA', 'UAG', 'UGA'], None)]

PROTEIN_BY_CODON = {codon: protein for codons, protein in CATALOG for codon in codons}


def proteins(strand: str) -> List[str]:
    protein_generator = (PROTEIN_BY_CODON[codon] for codon in wrap(strand, 3))
    return list(takewhile(lambda x: x, protein_generator))
