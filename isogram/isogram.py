def is_isogram(string):
    letters = [x.upper() for x in string if x.isalpha()]
    return len(letters) == len(set(letters))
