def flatten(iterable: list) -> list:
    result = []
    for x in iterable:
        if isinstance(x, (list, tuple)):
            result += flatten(x)
        elif x is not None:
            result.append(x)
    return result
