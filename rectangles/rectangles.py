from typing import Iterator, List, Tuple
Point = Tuple[int, int]


class Diagram:
    def __init__(self, schema: List[str]) -> None:
        self.schema = schema
        self.height = len(schema)
        self.width = len(schema[0]) if self.height > 0 else 0

    def is_rectangle(self, this: Point, that: Point) -> bool:
        if not all(self.schema[m][n] == '+' for m, n in [(this[0], that[1]), (that[0], this[1])]):
            return False
        for m in range(this[0] + 1, that[0]):
            if not all(self.schema[m][n] in '|+' for n in [this[1], that[1]]):
                return False
        for n in range(this[1] + 1, that[1]):
            if not all(self.schema[m][n] in '-+' for m in [this[0], that[0]]):
                return False
        return True

    def get_vertices(self, corner: Point = (-1, -1)) -> Iterator[Point]:
        for m in range(corner[0] + 1, self.height):
            for n in range(corner[1] + 1, self.width):
                if self.schema[m][n] == '+':
                    yield (m, n)


def rectangles(ascii_diagram: List[str]) -> int:
    diagram = Diagram(ascii_diagram)
    rectangle_count = 0
    for this_point in diagram.get_vertices():
        for that_point in diagram.get_vertices(this_point):
            if diagram.is_rectangle(this_point, that_point):
                rectangle_count += 1
    return rectangle_count
