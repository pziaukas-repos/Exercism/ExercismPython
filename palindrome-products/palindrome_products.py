from typing import Generator, Optional, Set, Tuple

PalindromeWithFactors = Tuple[Optional[int], Set[Tuple[int, int]]]


def is_palindrome(number: int) -> bool:
    number_string = str(number)
    return number_string == number_string[::-1]


def generate_palindromes(start: int, finish: int) -> Generator[int, None, None]:
    for length in range(len(str(start)), len(str(finish)) + 1):
        first_part = 10 ** ((length - 1) // 2)
        for part in map(str, range(first_part, 10 * first_part)):
            palindrome = int(part + part[-(length % 2) - 1::-1])
            if start <= palindrome <= finish:
                yield palindrome


def get_palindrome_factors(min_factor: int, max_factor: int, reverse: bool = False) -> PalindromeWithFactors:
    if min_factor > max_factor:
        raise ValueError('Invalid range!')

    sequence = generate_palindromes(min_factor ** 2, max_factor ** 2)
    if reverse:
        sequence = reversed(list(sequence))
    for palindrome in sequence:
        factors_set = set()
        for m in range(min_factor, max_factor + 1):
            n, remainder = divmod(palindrome, m)
            if remainder == 0 and m <= n <= max_factor:
                factors_set.add((m, n))
        if len(factors_set) > 0:
            return palindrome, factors_set
    return None, set()


def largest(min_factor: int, max_factor: int) -> PalindromeWithFactors:
    return get_palindrome_factors(min_factor, max_factor, reverse=True)


def smallest(min_factor: int, max_factor: int) -> PalindromeWithFactors:
    return get_palindrome_factors(min_factor, max_factor)
