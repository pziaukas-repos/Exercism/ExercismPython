def convert(number: int) -> str:
    speech = [(3, 'Pling'), (5, 'Plang'), (7, 'Plong')]
    return ''.join(word for factor, word in speech if number % factor == 0) or str(number)
