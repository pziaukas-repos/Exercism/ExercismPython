from math import gcd
from textwrap import wrap
from typing import Callable

ord_a = ord('a')
m = 26


def shift(text: str, formula: Callable) -> str:
    def shift_char(x: str) -> str:
        if x.isdigit():
            return x
        sequence = ord(x.lower()) - ord_a
        return chr(formula(sequence) + ord_a)
    return ''.join(shift_char(x) for x in text if x.isalnum())


def coprime_key(code: Callable) -> Callable:
    def wrapper(text: str, a: int, b: int) -> str:
        if gcd(a, m) != 1:
            raise ValueError('Parameters "a" and "m" must be coprime!')
        return code(text, a, b)
    return wrapper


@coprime_key
def encode(plain_text: str, a: int, b: int) -> str:
    ciphered_text = shift(plain_text, formula=lambda seq: (a * seq + b) % m)
    return ' '.join(wrap(ciphered_text, 5))


@coprime_key
def decode(ciphered_text: str, a: int, b: int) -> str:
    # the inverse of a is equivalent to a^(phi(m) - 1) = a^11
    plain_text = shift(ciphered_text, formula=lambda seq: a**11 * (seq - b) % m)
    return plain_text
